#
# Install a Ruby package
#
# :param FILES: list of files to install as a project_name module
# :type FILES: list
# :param DIRECTORIES: list of directories as a project_name module
# :type DIRECTORIES: list
# :param ENTRY_POINTS: list of entry points on the form of 'nodename = package:function'
# :type ENTRY_POINTS: list

macro(ament_ruby_install_package)
  _ament_cmake_ruby_register_environment_hook()
  _ament_cmake_ruby_install_package(${ARGN})
endmacro()

function(_ament_cmake_ruby_install_package)
  cmake_parse_arguments(ARG "" "" "ENTRY_POINTS;FILES;DIRECTORIES" ${ARGN})
  if(ARG_UNPARSED_ARGUMENTS)
    message(FATAL_ERROR "ament_ruby_install_module() called with unused "
      "arguments: ${ARG_UNPARSED_ARGUMENTS}")
  endif()

  _ament_cmake_ruby_install_module(FILES ${ARG_FILES} DIRECTORIES ${ARG_DIRECTORIES} DESTINATION_SUFFIX ${PROJECT_NAME})
  
  foreach(entrypoint IN LISTS ARG_ENTRY_POINTS)
    string(REPLACE " " "" ep_s ${entrypoint})
    string(REPLACE "=" ";" ep_s ${ep_s})
    string(REPLACE ":" ";" ep_s "${ep_s}")
    list(LENGTH ep_s len)
    if(NOT len EQUAL 3)
      message(FATAL_ERROR "Entry point should have the form 'nodename = package:function' got '${entrypoint}' ${len}")
    endif()
    list(GET ep_s 0 nodename)
    list(GET ep_s 1 package_name)
    list(GET ep_s 2 entry_point)

    configure_file(${__AMENT_CMAKE_RUBY_DIR}/templates/entry_point.rb.in ${CMAKE_CURRENT_BINARY_DIR}/${nodename})
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${nodename} DESTINATION lib/${PROJECT_NAME} PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
    GROUP_EXECUTE GROUP_READ WORLD_READ WORLD_EXECUTE)
  endforeach()
    
endfunction()

