#
# Install a Ruby module
#
# :param DESTINATION_SUFFIX: the base package to install the module to
#   (default: empty, install as a top level module)
# :type DESTINATION_SUFFIX: string
# :param FILES: list of files to install
# :type FILES: list
# :param DIRECTORIES: list of directories
# :type DIRECTORIES: list

macro(ament_ruby_install_module)
  _ament_cmake_ruby_register_environment_hook()
  _ament_cmake_ruby_install_module(${ARGN})
endmacro()

function(_ament_cmake_ruby_install_module)
  cmake_parse_arguments(ARG "" "DESTINATION_SUFFIX" "FILES;DIRECTORIES" ${ARGN})
  if(ARG_UNPARSED_ARGUMENTS)
    message(FATAL_ERROR "ament_ruby_install_module() called with unused "
      "arguments: ${ARG_UNPARSED_ARGUMENTS}")
  endif()

  if(NOT IS_ABSOLUTE "${module_file}")
    set(module_file "${CMAKE_CURRENT_LIST_DIR}/${module_file}")
  endif()
  if(NOT EXISTS "${module_file}")
    message(FATAL_ERROR "ament_ruby_install_module() the Ruby module file "
      "'${module_file}' doesn't exist")
  endif()

  set(destination lib/ruby/${PROJECT_NAME})
  if(ARG_DESTINATION_SUFFIX)
    set(destination "${destination}/${ARG_DESTINATION_SUFFIX}")
  endif()

  install(
    FILES ${ARG_FILES}
    DESTINATION "${destination}"
    )
  foreach(directory IN LISTS ARG_DIRECTORIES)
    install(
      DIRECTORY "${directory}"
      DESTINATION "${destination}"
      )
  endforeach()
    
  if(destination IN_LIST AMENT_CMAKE_RUBY_INSTALL_INSTALLED_NAMES)
  message(FATAL_ERROR "ament_ruby_install_module() a Ruby module file "
    "or package with the same name '${destination}' has been installed before")
  endif()
  list(APPEND AMENT_CMAKE_RUBY_INSTALL_INSTALLED_NAMES "${destination}")
  set(AMENT_CMAKE_RUBY_INSTALL_INSTALLED_NAMES
    "${AMENT_CMAKE_RUBY_INSTALL_INSTALLED_NAMES}" PARENT_SCOPE)

endfunction()
