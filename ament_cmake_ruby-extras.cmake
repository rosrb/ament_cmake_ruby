# register environment hook for RUBYLIB once

set(__AMENT_CMAKE_RUBY_DIR ${CMAKE_CURRENT_LIST_DIR})

macro(_ament_cmake_ruby_register_environment_hook)
  if(NOT DEFINED _AMENT_CMAKE_RUBY_ENVIRONMENT_HOOK_REGISTERED)
    set(_AMENT_CMAKE_RUBY_ENVIRONMENT_HOOK_REGISTERED TRUE)

    find_package(ament_cmake_core QUIET REQUIRED)

    set(INSTALL_RUBY_DIR "${CMAKE_INSTALL_PREFIX}/lib/ruby/${PROJECT_NAME}")
    ament_environment_hooks(${__AMENT_CMAKE_RUBY_DIR}/env_hook/ruby.sh.in)
  endif()
endmacro()

include("${ament_cmake_ruby_DIR}/ament_ruby_install_module.cmake")
include("${ament_cmake_ruby_DIR}/ament_ruby_install_package.cmake")
