ament_cmake_ruby
================

This package provides cmake functions to create ROS package with Ruby program and libraries. You can find examples in [rclrb examples](https://gitlab.com/rosrb/rclrb_examples).

It provides two functions:

* `ament_ruby_install_package` allows to install ROS node written in Ruby, and setup the environment.
* `ament_ruby_install_module` allows to install Ruby modules and make them available in a ROS environment.
